//
//  GameScene.swift
//  p09-ReverseSwipe
//
//  Created by Tanmay Kale on 4/28/16.
//  Copyright (c) 2016 tkale1. All rights reserved.
//

import SpriteKit

var gameMode="Arcade Mode"
var gameNo=0
class GameScene: SKScene {

    override func didMove(to view: SKView) {
        /* Setup your scene here */
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        //Adding Game label
        let gameName = SKLabelNode(fontNamed:"Chalkduster")
        gameName.text = "Reverse Swipe"
        gameName.fontSize = 45
        gameName.fontColor = SKColor.black
        gameName.position = CGPoint(x:self.frame.midX, y:self.frame.midY+250)
        self.addChild(gameName)

        //setup Background
        self.backgroundColor = UIColor(red: 74/255, green: 125/255, blue: 240.0/255, alpha: 1.0)
        
//        let background = SKSpr    iteNode(imageNamed: "background2.jpg")
//        background.zPosition = -1
//        background.position = CGPointMake(frame.size.width / 2, frame.size.height/2)
//        addChild(background)

        //adding right arrow
        let arrowName = "Rightmain"
        let rightarrow = UIImage(named: arrowName)
        let addrightarrow = UIImageView(image: rightarrow!)
        addrightarrow.frame = CGRect(x:screenWidth/5, y: screenHeight/3, width: screenWidth/4, height: 60)
        view.addSubview(addrightarrow)
        
        //Adding left arrow
        let arrowName1 = "Leftmain"
        let leftarrow = UIImage(named: arrowName1)
        let addleftarrow = UIImageView(image: leftarrow!)
        addleftarrow.frame = CGRect(x:screenWidth/1.8, y: screenHeight/3, width: screenWidth/4, height: 60)
        view.addSubview(addleftarrow)
        
        //adding Up arrow
        let arrowName2 = "Upmain"
        let uparrow = UIImage(named: arrowName2)
        let adduparrow = UIImageView(image: uparrow!)
        adduparrow.frame = CGRect(x:screenWidth/2-35, y: screenHeight/3+50, width: screenWidth/4-10, height: 60)
        view.addSubview(adduparrow)
        
        //adding down arrow
        let arrowName3 = "Downmain"
        let downarrow = UIImage(named: arrowName3)
        let adddownarrow = UIImageView(image: downarrow!)
        adddownarrow.frame = CGRect(x:screenWidth/2-35, y: screenHeight/3-50, width: screenWidth/4-10, height: 60)
        view.addSubview(adddownarrow)
        
        //adding Button for game type
        let Arcade = UIButton()
        let playImg = UIImage(named:"Arcade1.png")
        Arcade.setTitle("ARCADE", for: UIControlState())
        Arcade.setTitleColor(UIColor.blue, for: UIControlState())
        Arcade.setImage(playImg, for: UIControlState())
        Arcade.frame = CGRect(x:screenWidth/2-70, y: screenHeight/1.8-10, width: 140, height: 50)
        Arcade.addTarget(self, action: #selector(GameScene.buttonAction(_:)), for: UIControlEvents.touchUpInside)
        view.addSubview(Arcade)
        
        
        let Timer = UIButton()
        let timerImg = UIImage(named:"Timer1.png")
        Timer.setTitle("TIMER", for: UIControlState())
        Timer.setTitleColor(UIColor.blue, for: UIControlState())
        Timer.setImage(timerImg, for: UIControlState())
        Timer.frame = CGRect(x:screenWidth/2-70, y: screenHeight/1.8+55, width: 140, height: 50)
        Timer.addTarget(self, action: #selector(GameScene.buttonAction(_:)), for: UIControlEvents.touchUpInside)
        view.addSubview(Timer)
        
    }
    
    func buttonAction(_ TheButton : UIButton!) {
        //To get the name of the mode
        if let text = TheButton.titleLabel?.text{
            if(text=="ARCADE")
            {
                gameMode="ARCADE MODE"
                gameNo=1
            }
            else{
                gameMode="TIME ATTACK"
                gameNo=2
            }
        }
        
        self.view!.window!.rootViewController!.performSegue(withIdentifier: "Playsegue", sender: self)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
       /* Called when a touch begins */
        
   
    }
   
    override func update(_ currentTime: TimeInterval) {
        /* Called before each frame is rendered */
    }
}
