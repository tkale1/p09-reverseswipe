//
//  GameScreen.swift
//  p09-ReverseSwipe
//
//  Created by Tanmay Kale on 5/13/16.
//  Copy right © 2016 tkale1. All rights reserved.
//


import UIKit
import SpriteKit
import AVFoundation

var Score=0

class GameScreen: UIViewController {
    var Lives=3
    //setting background
     @IBOutlet var gameover: UILabel!
    let addtopArrow = UIImageView()
    let addmidArrow = UIImageView()
    let addmainArrow = UIImageView()
    let currLives = UILabel()
    let currTime = UILabel()
    let currScore = UILabel()
    var i1=0
    var i2=0
    var i3=0
    var t1=0
    var t2=0
    var temp=0
    var audioPlayer:AVAudioPlayer!
    let audioFilePath = Bundle.main.path(forResource: "Buzz", ofType: "mp3")
    let CorrectSound = Bundle.main.path(forResource: "CSound", ofType: "mp3")
    
    var startTime = TimeInterval()
    var timer = Timer()
    var gameTime:Double = 30
    var blurEffect = UIBlurEffect()
    var blurEffectView = UIVisualEffectView()
    let mainImage: [UIImage] = [
        UIImage(named: "Up.png")!,
        UIImage(named: "Down.png")!,
        UIImage(named: "Left.png")!,
        UIImage(named: "Right.png")!,
        UIImage(named: "Rup.png")!,
        UIImage(named: "Rdown.png")!,
        UIImage(named: "Rleft.png")!,
        UIImage(named: "Rright.png")!
        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("game Mode  : ",gameMode)
        print("Mode  No : ",gameNo)
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        self.view.backgroundColor = UIColor(red: 74/255, green: 125/255, blue: 240.0/255, alpha: 1.0)
         i1 = Int(arc4random_uniform(8))
         i2 = Int(arc4random_uniform(8))
         i3 = Int(arc4random_uniform(8))
        
        blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        blurEffectView = UIVisualEffectView(effect: blurEffect)
        //always fill the view
        blurEffectView.frame = self.view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]


        let Mode = UILabel(frame: CGRect(x: screenWidth/2-120,y: 30 , width: 240, height: 30))
        Mode.textAlignment = NSTextAlignment.center
        Mode.font = UIFont(name: "Chalkduster", size: 28.0)
        Mode.text = gameMode
        self.view.addSubview(Mode)
        
        if(gameNo==1){
            
            let Lives = UILabel(frame: CGRect(x: 25, y: 80, width: 100, height: 40))
            Lives.textAlignment = NSTextAlignment.center
            Lives.font = UIFont(name: "Chalkduster", size: 20.0)
            Lives.text = "Lives : "
            self.view.addSubview(Lives)
        
            currLives.frame = CGRect(x: 100, y: 80, width: 60, height: 40)
            currLives.textAlignment = NSTextAlignment.center
            currLives.font = UIFont(name: "Chalkduster", size: 20.0)
            currLives.text = "3"
            self.view.addSubview(currLives)
        }
        else{
            startGame()
            let Timer = UILabel(frame: CGRect(x: 25, y: 80, width: 100, height: 40))
            Timer.textAlignment = NSTextAlignment.center
            Timer.font = UIFont(name: "Chalkduster", size: 20.0)
            Timer.text = "Time  : "
            self.view.addSubview(Timer)
            
            currTime.frame = CGRect(x: 100, y: 80, width: 60, height: 40)
            currTime.textAlignment = NSTextAlignment.center
            currTime.font = UIFont(name: "Chalkduster", size: 20.0)
            currTime.text = "30"
            self.view.addSubview(currTime)
        }

        let ScoreLabel = UILabel(frame: CGRect(x: screenWidth/1.5-30, y: 80, width: 100, height: 40))
        ScoreLabel.textAlignment = NSTextAlignment.center
        ScoreLabel.font = UIFont(name: "Chalkduster", size: 20.0)
        ScoreLabel.text = "Score : "
        self.view.addSubview(ScoreLabel)
        
        currScore.frame = CGRect(x: screenWidth/1.3+15, y: 80, width: 60, height: 40)
        currScore.textAlignment = NSTextAlignment.center
        currScore.font = UIFont(name: "Chalkduster", size: 20.0)
        currScore.text = "0"
        self.view.addSubview(currScore)
        
//        let background = UIImage(named: "background2.jpg")
//        var back : UIImageView!
//        back = UIImageView(frame: view.bounds)
//        back.contentMode =  UIViewContentMode.ScaleAspectFill
//        back.clipsToBounds = true
//        back.image = background
//        back.center = view.center
//        view.addSubview(back)
//        self.view.sendSubviewToBack(back)
//
        mainImage[0].accessibilityIdentifier="up.png"
        mainImage[1].accessibilityIdentifier="down.png"
        mainImage[2].accessibilityIdentifier="left.png"
        mainImage[3].accessibilityIdentifier="right.png"
        mainImage[4].accessibilityIdentifier="Rup.png"
        mainImage[5].accessibilityIdentifier="Rdown.png"
        mainImage[6].accessibilityIdentifier="Rleft.png"
        mainImage[7].accessibilityIdentifier="Rright.png"

        addtopArrow.image=(mainImage[i1])
        addtopArrow.frame = CGRect(x:screenWidth/2-25, y: screenHeight/5, width: 50, height: 35)
        view.addSubview(addtopArrow)
        
        addmidArrow.image=(mainImage[i2])
        addmidArrow.frame =  CGRect(x:screenWidth/2-37.5, y: screenHeight/3, width: 75, height: 60)
        view.addSubview(addmidArrow)
        
        addmainArrow.image=(mainImage[i3])
        addmainArrow.frame = CGRect(x:screenWidth/2-60, y: screenHeight/2, width: 120, height: 100)
        view.addSubview(addmainArrow)
        temp=i3
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(GameScreen.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(GameScreen.respondToSwipeGesture(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(GameScreen.respondToSwipeGesture(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(GameScreen.respondToSwipeGesture(_:)))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        self.view.addGestureRecognizer(swipeUp)
        
        
    }
    func startGame() {
        
        let aSelector : Selector = #selector(GameScreen.updateTime)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: aSelector, userInfo: nil, repeats: true)
        startTime = Date.timeIntervalSinceReferenceDate
        
    }
    
    func updateTime() {
        let currentTime = Date.timeIntervalSinceReferenceDate
        var elapsedTime = currentTime - startTime
        let seconds = gameTime-elapsedTime
        if seconds > 0 {
            elapsedTime -= TimeInterval(seconds)
            print("\(Int(seconds))")
            currTime.text = String((Int(seconds)))
            
        } else {
            self.callGameOver()
//            gameover.bringSubviewToFront(self.view)
//            gameover.hidden = false
//            self.view.addSubview(blurEffectView)
            timer.invalidate()
        }
    }
    
    func callGameOver() {
        performSegue(withIdentifier: "gameoverSegue", sender: self)
        //self.view!.window!.rootViewController!.performSegueWithIdentifier("gameoverSegue", sender: self)
    }
    
    func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            //if(swipeGestureUISwipeGestureRecognizerDirection.Right && )
            
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                if(mainImage[i3].accessibilityIdentifier=="right.png" || mainImage[i3].accessibilityIdentifier=="Rleft.png" && Lives>0)
                {
                    
                    if audioFilePath != nil {
                        
                        let audioFileUrl = URL(fileURLWithPath: CorrectSound!)
                        
                        audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                        audioPlayer.play()
                        print("Audio found")
                    } else {
                        print("audio file is not found")
                    }
                    
                    Score += 1;
                    currScore.text = NSString(format:"%d", Score) as String
                    print("Swiped right \t Score : ",Score)
                    
                    i3=i2
                    addmainArrow.image=(mainImage[i2])
                    view.addSubview(addmainArrow)
                    
                    i2=i1
                    addmidArrow.image=(mainImage[i2])
                    view.addSubview(addmidArrow)
                    
                    i1 = Int(arc4random_uniform(8))
                    
                    addtopArrow.image=(mainImage[i1])
                    view.addSubview(addtopArrow)
                    temp=i2
                }
                else
                {
                    if(Lives==0 && gameNo==1)
                    {
                        print("Game Over")
                        currLives.text = "0"
                        self.callGameOver()
//                        gameover.hidden = false
//                        self.view.addSubview(blurEffectView)
//                        blurEffectView.bringSubviewToFront(gameover)
                    }
                    else
                    {
                        Lives -= 1;
                        print("Wrong Swipe \t Lives Left : ",Lives)
                        currLives.text = NSString(format:"%d", Lives) as String
                        if audioFilePath != nil {
                            
                            let audioFileUrl = URL(fileURLWithPath: audioFilePath!)
                            
                            audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                            audioPlayer.play()
                            print("Audio found")
                        } else {
                            print("audio file is not found")
                        }
                    }
                }
            case UISwipeGestureRecognizerDirection.down:
                if(mainImage[i3].accessibilityIdentifier=="down.png" || mainImage[i3].accessibilityIdentifier=="Rup.png" && Lives>0)
                {
                    
                    if audioFilePath != nil {
                        
                        let audioFileUrl = URL(fileURLWithPath: CorrectSound!)
                        
                        audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                        audioPlayer.play()
                        print("Audio found")
                    } else {
                        print("audio file is not found")
                    }
                    
                    Score += 1;
                    print("Swiped Down \t Score : ",Score)
                    currScore.text = NSString(format:"%d", Score) as String
                    i3=i2
                    addmainArrow.image=(mainImage[i2])
                    view.addSubview(addmainArrow)
                    
                    i2=i1
                    addmidArrow.image=(mainImage[i2])
                    view.addSubview(addmidArrow)
                    
                    i1 = Int(arc4random_uniform(8))
                    
                    addtopArrow.image=(mainImage[i1])
                    view.addSubview(addtopArrow)
                    temp=i2
                }
                else
                {
                    if(Lives==0 && gameNo==1)
                    {
                        print("Game Over")
                        currLives.text="0"
                        self.callGameOver()
//                        gameover.hidden = false
//                        self.view.addSubview(blurEffectView)
//                        blurEffectView.bringSubviewToFront(gameover)
                    }
                    else
                    {
                        Lives -= 1;
                        print("Wrong Swipe \t Lives Left : ",Lives)
                        currLives.text = NSString(format:"%d", Lives) as String
                        if audioFilePath != nil {
                            
                            let audioFileUrl = URL(fileURLWithPath: audioFilePath!)
                            
                            audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                            audioPlayer.play()
                            print("Audio found")
                        } else {
                            print("audio file is not found")
                        }
                    }
                }

            case UISwipeGestureRecognizerDirection.left:
                if(mainImage[i3].accessibilityIdentifier=="left.png" || mainImage[i3].accessibilityIdentifier=="Rright.png" && Lives>0)
                {
                    
                    if audioFilePath != nil {
                        
                        let audioFileUrl = URL(fileURLWithPath: CorrectSound!)
                        
                        audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                        audioPlayer.play()
                        print("Audio found")
                    } else {
                        print("audio file is not found")
                    }
                    
                    Score += 1;
                    print("Swiped LEFT \t Score : ",Score)
                    currScore.text = NSString(format:"%d", Score) as String
                    
                    i3=i2
                    addmainArrow.image=(mainImage[i2])
                    view.addSubview(addmainArrow)
                    
                    i2=i1
                    addmidArrow.image=(mainImage[i2])
                    view.addSubview(addmidArrow)
                    
                    i1 = Int(arc4random_uniform(8))
                    
                    addtopArrow.image=(mainImage[i1])
                    view.addSubview(addtopArrow)
                    temp=i2
                }
                else
                {
                    if(Lives==0 && gameNo==1)
                    {
                        print("Game Over")
                        currLives.text="0"
                        self.callGameOver()
//                        gameover.hidden = false
//                        self.view.addSubview(blurEffectView)
//                        blurEffectView.bringSubviewToFront(gameover)
                    }
                    else
                    {
                        Lives -= 1;
                        print("Wrong Swipe \t Lives Left : ",Lives)
                        currLives.text = NSString(format:"%d", Lives) as String
                        if audioFilePath != nil {
                            
                            let audioFileUrl = URL(fileURLWithPath: audioFilePath!)
                            
                            audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                            audioPlayer.play()
                            print("Audio found")
                        } else {
                            print("audio file is not found")
                        }
                    }
                }
            case UISwipeGestureRecognizerDirection.up:
                if(mainImage[i3].accessibilityIdentifier=="up.png" || mainImage[i3].accessibilityIdentifier=="Rdown.png" && Lives>0)
                {
                    
                    if audioFilePath != nil {
                        
                        let audioFileUrl = URL(fileURLWithPath: CorrectSound!)
                        
                        audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                        audioPlayer.play()
                        print("Audio found")
                    } else {
                        print("audio file is not found")
                    }
                    
                    Score += 1;
                    print("Swiped UP \t Score : ",Score)
                    currScore.text = NSString(format:"%d", Score) as String
                    i3=i2
                    addmainArrow.image=(mainImage[i2])
                    view.addSubview(addmainArrow)
                    
                    i2=i1
                    addmidArrow.image=(mainImage[i2])
                    view.addSubview(addmidArrow)
                    
                    i1 = Int(arc4random_uniform(8))
                    
                    addtopArrow.image=(mainImage[i1])
                    view.addSubview(addtopArrow)
                    temp=i2
                }
                else
                {
                    if(Lives==0 && gameNo==1)
                    {
                        print("Game Over")
                        currLives.text="0"
                        self.callGameOver()
//                        gameover.hidden = false
//                        self.view.addSubview(blurEffectView)
//                        blurEffectView.bringSubviewToFront(gameover)
                    }
                    else
                    {
                        Lives -= 1;
                        print("Wrong Swipe \t Lives Left : ",Lives)
                        currLives.text = NSString(format:"%d", Lives) as String
                        if audioFilePath != nil {
                            
                            let audioFileUrl = URL(fileURLWithPath: audioFilePath!)
                            
                            audioPlayer = try? AVAudioPlayer(contentsOf: audioFileUrl)
                            audioPlayer.play()
                            print("Audio found")
                        } else {
                            print("audio file is not found")
                        }
                    }
                }

            default:
                break
            }
        }
    }
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
}
