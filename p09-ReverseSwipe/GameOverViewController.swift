//
//  GameOverViewController.swift
//  p09-ReverseSwipe
//
//  Created by Tanmay Kale on 5/16/16.
//  Copyright © 2016 tkale1. All rights reserved.
//
//
//
//

import UIKit
import SpriteKit

class GameOverViewController: UIViewController {

    @IBOutlet var Gohome: UIButton!
    let currLives = UILabel()
    let currTime = UILabel()
    let currScore = UILabel()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let screenSize: CGRect = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        self.view.backgroundColor = UIColor(red: 74/255, green: 125/255, blue: 240.0/255, alpha: 1.0)
        
        //Adding Buton to perfrm segue to the ViewController
        let Home = UIButton()
        let homeImg = UIImage(named:"Home1.png")
        Home.setTitle("HOME", for: UIControlState())
        Home.setTitleColor(UIColor.blue, for: UIControlState())
        Home.setImage(homeImg, for: UIControlState())
        Home.frame = CGRect(x:screenWidth/2-45, y: screenHeight/2-50, width: 100, height: 100)
        Home.addTarget(self, action: #selector(GameOverViewController.buttonPressed(_:)), for: UIControlEvents.touchUpInside)
        view.addSubview(Home)
        
        
        //Setting the Labels and Button
        let Mode = UILabel(frame: CGRect(x: screenWidth/2-125,y: screenHeight/3.7 , width: 250, height: 40))
        Mode.textAlignment = NSTextAlignment.center
        Mode.font = UIFont(name: "Chalkduster", size: 30.0)
        Mode.text = gameMode
        self.view.addSubview(Mode)
        
        let ScoreLabel = UILabel(frame: CGRect(x: screenWidth/2-80,y: screenHeight/2-100 , width: 100, height: 40))
        ScoreLabel.textAlignment = NSTextAlignment.center
        ScoreLabel.font = UIFont(name: "Chalkduster", size: 20.0)
        ScoreLabel.text = "Score : "
        self.view.addSubview(ScoreLabel)
        
        print("Score : ",Score)
        currScore.frame = CGRect(x: screenWidth/2+10,y: screenHeight/2-100 , width: 50, height: 40)
        currScore.textAlignment = NSTextAlignment.center
        currScore.font = UIFont(name: "Chalkduster", size: 20.0)
        currScore.text = NSString(format:"%d", Score) as String
        self.view.addSubview(currScore)
    }

    @IBAction func buttonPressed(_ sender: AnyObject) {
        //self.presentedViewController.presentViewController: activityController animated: YES completion:nil
        performSegue(withIdentifier: "MainScreensegue", sender: self)
        //self.view!.window!.rootViewController!.performSegueWithIdentifier("gameoverSegue", sender: self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
