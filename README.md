# **REVERSES SWIPE** #
DESCRIPTION:

Swipe the black arrows in the same direction, swipe the zebra arrows in the opposite direction. Sounds like an easy task? It's easy to play but hard to master.

• Simple controls

• 2 modes to play: ARCADE MODE, TIMER MODE

• Catchy and entertaining sound effects

REQUIREMENTS: iOS 8.0 or later, Compatible with iPhone, iPad and iPod touch
Language: English


![MainScreen.png](https://bitbucket.org/repo/5ky5nE/images/3693638789-MainScreen.png)

![ArcadeMode.png](https://bitbucket.org/repo/5ky5nE/images/1607374165-ArcadeMode.png)

![GameOver.png](https://bitbucket.org/repo/5ky5nE/images/761758814-GameOver.png)